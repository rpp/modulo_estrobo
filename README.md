Módulo estroboscópico
=====================

Programa para controle estroboscópico utilizando Arduino para aplicações em física de ondas mecânicas.

Visite o site oficial do projeto Laboratório de Física e Música do Centro de Tecnologia Acadêmica em http://cta.if.ufrgs.br/projects/laboratorio-fisica-e-musica/wiki

Este programa está disponível sob os termos da Licença GPL 3.0.

Autores: Rafael Pezzi, Renan Bohrer da Silva e Flavio Depaoli


