#include <LiquidCrystal.h>
LiquidCrystal lcd(1, 0, 5, 4, 3, 2);

// Definicao dos botoes de troca de frequencia e seus pinos
int bot_SOMA_0_1 = 9;
int bot_SOMA_1 = 8;
int bot_SOMA_10 = 7;
int bot_SUB_0_1 = 10;
int bot_SUB_1 = 11;
int bot_SUB_10 = A1;
int bot_DOBRO = 6;
int bot_METADE = A0;

// Pino de acionamento da fonte de luz estroboscopica
int led = 13;

long debounceDelay = 250; 
unsigned int T_strob = 20; // Proporcional ao tempo que o LED fica acesso durante cada período
float F_strob=120;  //Frequência inicial
unsigned int OC;

void setup() {                
  cli();           // disable all interrupts
  TCCR1A = 0;      // set entire TCCR1A register to 0
  TCCR1B = 0;  // mesmo para TCCR1B
  TCNT1  = 0;  // Inicia a contagem do zero

  //Configura o registrador de comparação
  OCR1A = 16000000.0/(F_strob*1024)-1;;
  OC= OCR1A - T_strob;
  OCR1B = OC;
  TCCR1A |= (1 << WGM10)|(1 << WGM11);   // Modo 15 - fast PWM 1
  TCCR1B |= (1 << WGM12)|(1 << WGM13);   // Modo 15 - fast PWM 1
  TCCR1B |= (1 << CS12) | (1 << CS10);    // 1024 prescaler
  TIMSK1 |= (1 << OCIE1B);  // Ativa interrupção de comparação B com contador
  TIMSK1 |= (1 << OCIE1A);  // Ativa interrupção de comparação A com contador
  sei();             // Ativa as interrupções

  lcd.begin(16, 2);
//  Serial.begin(9600); // Comunicaçao seria precisa usar pinos diferentes do LCD
  pinMode(led, OUTPUT);  


// Configurando pinos dos botoes como entrada
// e ativando os resistores pull-up
  pinMode(bot_SOMA_0_1, INPUT);
  digitalWrite(bot_SOMA_0_1,HIGH);

  pinMode(bot_SOMA_1, INPUT);
  digitalWrite(bot_SOMA_1,HIGH);

  pinMode(bot_SOMA_10, INPUT); 
  digitalWrite(bot_SOMA_10,HIGH);

  pinMode(bot_SUB_0_1, INPUT); 
  digitalWrite(bot_SUB_0_1,HIGH);

  pinMode(bot_SUB_1, INPUT); 
  digitalWrite(bot_SUB_1,HIGH);

  pinMode(bot_SUB_10, INPUT); 
  digitalWrite(bot_SUB_10,HIGH);

  pinMode(bot_DOBRO, INPUT); 
  digitalWrite(bot_DOBRO,HIGH);

  pinMode(bot_METADE = A0, INPUT); 
  digitalWrite(bot_METADE = A0,HIGH);

  
}

void loop() {

// Testa se cada botao esta precionsado : executa acao correspondente
    if (!digitalRead(bot_METADE)){
      F_strob/=2;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_DOBRO)){
      F_strob*=2;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_SOMA_0_1)){
      F_strob+=0.1;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_SOMA_1)){
      F_strob+=1.0;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_SOMA_10)){
      F_strob+=10.0;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_SUB_0_1)){
      F_strob-=0.1;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_SUB_1)){
      F_strob-=1.0;
      delay(debounceDelay);
    }
    if (!digitalRead(bot_SUB_10)){
      F_strob-=10.0;
      delay(debounceDelay);
    }

// ajusta valores do contador
  //    cli();
      OCR1A=16000000.0/(F_strob*1024)-1;
      OC= OCR1A;
      OC -= T_strob;
      OCR1B = OC;
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Freq (Hz):");
      lcd.setCursor(0,1);
      lcd.print(F_strob);
      delay(50);
//      sei();
      
}


// rotinas das interrupções que ligam e desligam os leds utilizando o timer1 
ISR(TIMER1_COMPB_vect)
{
digitalWrite(led, HIGH);
}


ISR(TIMER1_COMPA_vect)
{
digitalWrite(led, LOW);
}


